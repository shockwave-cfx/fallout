function CreateGrid(rows, cols)
  local grid = {}

  for row = 1, rows do
    for col = 1, cols do
      table.insert(grid, {
        row = row,
        col = col
      })
    end
  end

  return grid
end

function table.includes(xs, val)
  for _,x in pairs(xs) do
    if x == val then
      return true
    end
  end
  return false
end
