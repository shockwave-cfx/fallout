resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

client_script 'client/utils.lua'
client_script 'client/main.lua'

server_script 'server/utils.lua'
server_script 'server/main.lua'

client_script 'config.lua'
server_script 'config.lua'
